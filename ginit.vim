"set guifont=MesloLGM\ NF:h9
"https://www.reddit.com/r/neovim/comments/9n7sja/liga_source_code_pro_is_not_a_fixed_pitch_font/

let s:fontsize = 9
execute "GuiFont! MesloLGM NF:h" . s:fontsize
function! AdjustFontSize(amount)
        let s:fontsize = s:fontsize+a:amount
endfunction
