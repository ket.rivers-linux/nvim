# nvim

## Requirements
neovim

git

## Config installation (**LUA**)

1. Clone packer.vim

```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

2. Clone my config from gitlab.com

```
git clone https://gitlab.com/ket.rivers-linux/nvim.git ~/.config
```

3. Install the plugins

Start nvim, then run `:PackerInstall`

## File structure of `~/.config/nvim`

<pre>
~/.config/nvim/
|-- /colors/
|-- /lua/
|   |-- plugins.lua
|   |-- options.lua
|-- init.lua
</pre>


## windows steps

* Packer install

```
git clone https://github.com/wbthomason/packer.nvim\
 "$env:LOCALAPPDATA\nvim-data\site\pack\packer\start\packer.nvim"
```

## <s>vimplug</s>

## colors

https://github.com/kyoz/purify
