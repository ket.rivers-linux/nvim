-- Themes
vim.cmd[[colorscheme dracula]]
require('lualine').setup {
        options = {
                -- ...
                theme = 'dracula-nvim'
                -- ...
                }
        }

-- Options
local set = vim.opt	-- create variable set

-- Tab behaviour
set.tabstop = 8
set.shiftwidth = 8
set.softtabstop = 8
set.expandtab = true
set.wrap = true
set.textwidth = 78
set.formatoptions = tcqrnl	-- what is this for????

-- Line numbers
set.number = true
set.relativenumber = true
set.cursorline = true

-- Cursor motion
set.scrolloff = 3
set.backspace = {"indent", "eol", "start"}

-- Copy/Paste from outside of nvim
set.clipboard = {"unnamed", "unnamedplus"}
