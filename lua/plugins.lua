return require('packer').startup(function()
	use 'wbthomason/packer.nvim'
	use {
		'nvim-lualine/lualine.nvim',
		requires = {'kyazdani42/nvim-web-devicons', opt = true}
	}
	use 'Mofiqul/dracula.nvim'
	use "fladson/vim-kitty"
        use {"ellisonleao/glow.nvim", branch = 'main'}
end)
